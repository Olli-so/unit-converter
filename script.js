LENGTH_CONVERSION = {
    "mile":  1609.344,
    "km"  :  1000,
    "hm"  :  100,
    "dam" :  10,
    "m"   :  1,
    "yd"  :  0.9144, 
    "ft"  :  0.3048,
    "dm"  :  0.1,
    "in"  :  0.0254,
    "cm"  :  0.01,
    "mm"  :  0.001,
}
MASS_CONVERSION = {
    "ton": 907184.74,
    "kg": 1000,
    "lb": 453.59237,
    "hg": 100,
    "oz": 28.349523125,
    "dag": 10, 
    "g": 1,
    "dm": 0.1,
    "dr": 0.06479891,
    "cg": 0.01,
    "mg": 0.001,
}
function copyValue(elementID) {
    const elementValue = document.getElementById(elementID)
    elementValue.select()
    navigator.clipboard.writeText(elementValue.value)
}
function lengthOnClick() {
    const length = document.getElementById("length-input").value
    const inputUnit = document.getElementById("length-input-dropdown").value
    const outputUnit = document.getElementById("length-output-dropdown").value
    const output = convertLength(length, inputUnit, outputUnit)
    document.getElementById("length-output").value = output
}
function convertLength(length, inputUnit, outputUnit) {
    const inUnitRate = LENGTH_CONVERSION[inputUnit]
    const outUnitRate = LENGTH_CONVERSION[outputUnit]
    let conversionRate = inUnitRate / outUnitRate
    return length * conversionRate;
}
function massOnClick() {
    const mass = document.getElementById("mass-input").value
    const inputUnit = document.getElementById("mass-input-dropdown").value
    const outputUnit = document.getElementById("mass-output-dropdown").value
    const output = convertMass(mass, inputUnit, outputUnit)
    document.getElementById("mass-output").value = output
}
function convertMass(mass, inputUnit, outputUnit) {
    const inUnitRate = MASS_CONVERSION[inputUnit]
    console.log(inUnitRate)
    const outUnitRate = MASS_CONVERSION[outputUnit]
    console.log(outUnitRate)
    let conversionRate = inUnitRate / outUnitRate
    return mass * conversionRate
}